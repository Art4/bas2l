#!/usr/bin/env bash

echo "Bas2l - Backup Server to Local"
echo "------------------------------"
echo ""

while [ $# -gt 0 ]; do
  case "$1" in
    --interval=*)
      JOBINTERVAL="${1#*=}"
      ;;
    --source-server=*)
      SOURCE_SERVER="${1#*=}"
      ;;
    --source-dir=*)
      SOURCE_PATH="${1#*=}"
      ;;
    --target-dir=*)
      TARGET_PATH="${1#*=}"
      ;;
    --datetimetz=*)
      CURRENT_DATETIME="${1#*=}"
      ;;
    --pre-hook=*)
      PRE_HOOK_SCRIPT="${1#*=}"
      ;;
    *)
      printf "! Fehler: \`$1\` ist kein gültiger Parameter.\n"
      exit 1
  esac
  shift
done

PROJECT_DIR="$( cd "$(dirname "$0")" ; pwd -P )/.."
if [ -z $JOBINTERVAL ]; then echo "Bitte gib mit --interval=hourly|daily|weekly|monthly den Job Interval an"; exit 1; fi
if [ -z $SOURCE_SERVER ]; then echo "Bitte gib mit --source-server=user@server den Server an, der gesichert werden soll"; exit 1; fi
if [ -z $SOURCE_PATH ]; then echo "Bitte gib mit --source-dir=/absolute/path den absoluten Pfad des zu sichernden Ordners an"; exit 1; fi
if [ -z $TARGET_PATH ]; then echo "Bitte gib mit --target-dir=/absolute/path den absoluten Pfad an, wo das Backup gespeichert werden soll"; exit 1; fi
if [ -z $CURRENT_DATETIME ]; then CURRENT_DATETIME="$( date +"%Y%m%d-%H%M%S%z" )"; fi

echo "Date:       "$CURRENT_DATETIME
echo ""
echo "Source:"
echo "- Server:   "$SOURCE_SERVER
echo "- Path:     "$SOURCE_PATH
echo "- Pre-Hook: "$PRE_HOOK_SCRIPT
echo ""
echo "Target:"
echo "- Path:     "$TARGET_PATH
echo ""

if [[ ! -d $TARGET_PATH ]]; then
    printf "! Target-Path ist nicht vorhanden.\n";
    printf "Erstelle Target-Path...\n";
    mkdir -p $TARGET_PATH;
    echo "Erledigt."
    echo ""
fi

echo "Teste SSH-Verbindung zum Source-Server $SOURCE_SERVER..."
ssh -o ConnectTimeout=5 -o BatchMode=yes -p22 $SOURCE_SERVER "ls -al $SOURCE_PATH" || { echo "Keine SSH-Verbinung zum Server. Abbruch!" ; exit 1; }
echo "Verbindung zum Server funktioniert"
echo ""

echo "Erstelle rsnapshot Config Datei in $TARGET_PATH/rsnapshot.conf..."

cp "$PROJECT_DIR/config/rsnapshot.template.conf" "$TARGET_PATH/rsnapshot.conf" || { echo "Abbruch!" ; exit 1; }

# Platzhalter ersetzen
sed -i -r "s|\{\{TARGET_PATH\}\}|$TARGET_PATH|" "$TARGET_PATH/rsnapshot.conf" || { echo "Abbruch!" ; exit 1; }
sed -i -r "s|\{\{SOURCE_SERVER\}\}|$SOURCE_SERVER|" "$TARGET_PATH/rsnapshot.conf" || { echo "Abbruch!" ; exit 1; }
sed -i -r "s|\{\{SOURCE_PATH\}\}|$SOURCE_PATH|" "$TARGET_PATH/rsnapshot.conf" || { echo "Abbruch!" ; exit 1; }
sed -i -r "s|\{\{SOURCE_PATH_FOLDER\}\}|$(basename $SOURCE_PATH)|" "$TARGET_PATH/rsnapshot.conf" || { echo "Abbruch!" ; exit 1; }
echo "Erledigt."
echo ""

echo "Backup gestartet..."
echo ""

if [[ ! -z "$PRE_HOOK_SCRIPT" ]]; then
    echo "Führe Pre-Hook $PRE_HOOK_SCRIPT auf Source-Server aus..."
    ssh -p22 $SOURCE_SERVER "$PRE_HOOK_SCRIPT" || { echo "Abbruch!" ; exit 1; }
    echo "Erledigt."
    echo ""
fi

echo "Kopiere Daten via rsnapshot sync..."
$PROJECT_DIR/lib/rsnapshot-1.4.3 -c $TARGET_PATH/rsnapshot.conf sync || { echo "Abbruch!" ; exit 1; }
echo "Erledigt."
echo ""

echo "Rotiere Backup-Files via rsnapshot $JOBINTERVAL..."
$PROJECT_DIR/lib/rsnapshot-1.4.3 -c $TARGET_PATH/rsnapshot.conf $JOBINTERVAL || { echo "Abbruch!" ; exit 1; }
echo "Erledigt."
echo ""

echo "Backup am $CURRENT_DATETIME fertig erstellt."

exit 0;
