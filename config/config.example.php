<?php
return [
    'jobs' => [
        'example' => [
            'source_server' => 'user@example.com',
            'source_dir' => '/var/www/example',
            'target_dir' => '%PROJECT_DIR%/data/example',
            'token' => null, // [optional] null oder random string, der im Querystring gesetzt sein muss, um den Job ausführen zu dürfen. Bsp ?token=abc123
            'pre_hook_script' => null, // Bsp: 'bas2l-pre-backup.sh'
        ],
    ],
];
