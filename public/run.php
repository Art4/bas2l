<?php

error_reporting(\E_ALL | \E_STRICT);

set_error_handler(function($severity, $message, $file, $line) {
    if (!(error_reporting() & $severity)) {
        // This error code is not included in error_reporting
        return;
    }

    throw new \ErrorException($message, 0, $severity, $file, $line);
});

try {
    $projectDir = dirname(__DIR__);

    if (! is_file($projectDir.'/config/config.php')) {
        throw new \Exception('Die Datei `./config/config.php` muss zuerst erstellt werden. Kopiere `./config/config.example.com` nach `./config/config.php`.');
    }

    $config = require $projectDir.'/config/config.php';

    if (array_key_exists('interval', $_GET)) {
        $interval = strval($_GET['interval']);
    } else {
        $interval = '';
    }

    if (! in_array($interval, ['hourly', 'daily', 'weekly', 'monthly'])) {
        throw new \Exception('Bitte gib mit ?interval=<interval> einen gültigen Interval ("hourly", "daily", "weekly" oder "monthly") an.', 1);
    }

    if (array_key_exists('token', $_GET)) {
        $token = strval($_GET['token']);
    } else {
        $token = null;
    }

    if (array_key_exists('job', $_GET)) {
        $jobName = strval($_GET['job']);
    } else {
        $jobName = null;
    }

    $jobs = (array_key_exists('jobs', $config) && is_array($config['jobs'])) ? $config['jobs'] : [];

    if ($jobName === null or ! array_key_exists($jobName, $config['jobs'])) {
        $jobNames = implode(', ', array_keys($jobs));

        if ($jobNames !== '') {
            $jobNames = ' Gültige Jobs sind: ' . $jobNames;
        }

        throw new \Exception('Bitte gib mit ?job=jobname einen gültigen Job an.'.$jobNames, 1);
    }

    $jobConfig = $config['jobs'][$jobName];

    if (array_key_exists('token', $jobConfig) and $jobConfig['token'] !== null and strval($jobConfig['token']) !== $token) {
        throw new \Exception('Falscher Token!', 1);
    }

    $datetimetz = date('Ymd-HisO');

    if (! array_key_exists('source_server', $jobConfig) || ! is_string($jobConfig['source_server'])) {
        throw new \Exception(sprintf(
            'Im Job `%s` muss `source_server` (als string) definiert sein.',
            $jobName
        ), 1);
    }

    $sourceServer = strval($jobConfig['source_server']);

    if (! array_key_exists('source_dir', $jobConfig) || ! is_string($jobConfig['source_dir'])) {
        throw new \Exception(sprintf(
            'Im Job `%s` muss `source_dir` (als string) definiert sein.',
            $jobName
        ), 1);
    }

    $sourceDir = strval($jobConfig['source_dir']);

    if (! array_key_exists('target_dir', $jobConfig) || ! is_string($jobConfig['target_dir'])) {
        throw new \Exception(sprintf(
            'Im Job `%s` muss `target_dir` (als string) definiert sein.',
            $jobName
        ), 1);
    }

    $targetDir = strval($jobConfig['target_dir']);
    $targetDir = str_replace('%PROJECT_DIR%', $projectDir, $targetDir);

    if (! is_dir($targetDir)) {
        if (mkdir($targetDir, 0775) !== true) {
            $message = sprintf(
                'Das Zielverzeichnis `%s` konnte nicht gefunden werden. Anlegen des Verzeichnisses ist fehlgeschlagen.',
                $targetDir
            );

            throw new \Exception($message, 1);
        }
    }

    $preHookScript = null;

    if (array_key_exists('pre_hook_script', $jobConfig) and is_string($jobConfig['pre_hook_script'])) {
        $preHookScript = strval($jobConfig['pre_hook_script']);
    }

    if (! is_dir($targetDir.'/logs')) {
        mkdir($targetDir.'/logs', 0775);
    }

    $command = sprintf(
        '/bin/bash %s/bin/bas2l.sh --interval=%s --source-server=%s --source-dir=%s --target-dir=%s --datetimetz=%s%s 2>&1',
        $projectDir,
        $interval,
        $sourceServer,
        $sourceDir,
        $targetDir,
        $datetimetz,
        ($preHookScript !== null) ? ' --pre-hook='.$preHookScript : ''
    );

    exec($command, $log, $result);

    $logHandle = fopen($targetDir.'/logs/bas2l.log', 'a');

    foreach ($log as $logLine) {
        fwrite($logHandle, '['.$datetimetz.'] '.$logLine."\n");
    }

    fclose($logHandle);

    if ($result !== 0) {
        throw LoggerException::create($log, $result);
    }

    echo "Erledigt.";
} catch (\Exception $e) {
    echo "<h1>".get_class($e)."</h1>";
    echo "<h4>in " .$e->getFile() ." (line ".$e->getLine().")</h4>";
    echo "<h3>" .$e->getMessage() ."</h3>";
    echo "Details: " ."<br>";
    echo "<pre>";
    var_dump($e);
    echo "</pre>";

    if ($e instanceof LoggerException) {
        echo "Log: " ."<br>";
        echo "<pre>";
        var_dump($e->getLogAsString());
        echo "</pre>";
    }

    exit(1);
}

class LoggerException extends \Exception
{
    private $log = [];

    public static function create(array $log, int $code): LoggerException
    {
        $message = end($log);
        reset($log);

        $e = new self($message, $code);
        $e->log = $log;

        return $e;
    }

    public function getLogAsString(): string
    {
        $out = '';

        foreach ($this->log as $logLine) {
            $out .= $logLine."\n";
        }

        return $out;
    }
}

exit(0);
