# Bas2l - Backup Server to Local

Dieses Tool kann die verschiedenen Server aufrufen und lokale Backups davon erstellen.

### Voraussetzungen

- Bash
- SSH
- rsync
- PHP (optional)

### Cronjobs

Richte verschiedene Cronjobs für verschiedene Jobs und Zeitintervale ein:

```
https://example.com/bas2l/run.php?job=example&interval=hourly&token=secretstring
https://example.com/bas2l/run.php?job=example&interval=daily&token=secretstring
https://example.com/bas2l/run.php?job=example&interval=weekly&token=secretstring
https://example.com/bas2l/run.php?job=example&interval=montly&token=secretstring

https://example.com/bas2l/run.php?job=job2&interval=hourly
```

### Ablage

Die Backups werden lokal im Verzeichnis `data` (einstellbar) abgelegt und mit Hilfe von rsnapshot versioniert.

Die Backup-Struktur sieht dann so aus

```
./data/
  project_1/
    hourly.0/
    hourly.1/
    logs/
  project_2/
    hourly.0/
    hourly.1/
    logs/
```
